# MiniDisc
*A lightweight MultiPlay clone in Java*

## What is MiniDisc?
Currently in the semi-functional development phase, MiniDisc attempts to emulate the cue-list nature of [MutiPlay](http://www.da-share.com/software/multiplay/) for use in theatre and corporate use.

Using the JavaFX audio classes, the program should be able to cue up any type of audio file that your version of Java can play, and apply timed cues and fades to them.

The program will also, eventually, have a cart player interface, so that instead of a list of cues, a set of audio carts will be displayed which can be activated in any order at any time. There may even be hotkey support.

## Why not just use Multiplay?

Multiplay is for Windows. My netbook runs [Arch Linux](http://www.archlinux.org). I'm sure with a lot of effort, some fiddling with [Mono](http://www.mono-project.org) and [Wine](http://www.winehq.org) I could get it running - but I haven't managed to get that working yet, so I'll stick with this. 
Plus, it's a fun project.

Having said that, if you're on Windows, this is probably not the application for you. You should stick to the original and best Multiplay by AVD - https://www.audiovisualdevices.com.au/software/multiplay/


## One more thing...
Despite trying to *emulate* MultiPlay, there are a few things I have no intention of adding to this project. This includes, at least, serial or MIDI control and - in the immediate future anyway - support for multiple audio devices.

If any of these are things you'd like to see, and you have some programming knowledge, feel free to fork the project and work on it. I'd gladly merge your changes into the main codebase if you did!

## Requirements
 * Java 8
 * Sound Ouput Device