/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package net.angusi.sw.minidisc;

import javafx.application.Application;
import javafx.stage.Stage;
import net.angusi.sw.minidisc.gui.ModePicker;

public class MiniDisc extends Application {

    private static final String APPTITLE = "MiniDisc";
    private static final String APPVERSION = "0.0.2";
    private static MiniDisc handle;

    public static void main(String[] args) {

        System.out.println(APPTITLE+" version "+APPVERSION);

        System.out.println("Initialising JavaFX");
        launch(args);
    }

    public static String getAppTitle() {
        return APPTITLE;
    }

    public static String getAppVersion() {
        return APPVERSION;
    }

    public static void quitApplication() {
        System.out.println("Terminating.");
        System.exit(0);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ModePicker.getStage().show();
        handle = this;
    }

    public static MiniDisc getHandle() {
        return handle;
    }
}
