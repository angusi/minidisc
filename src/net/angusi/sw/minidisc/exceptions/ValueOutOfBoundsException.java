/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package net.angusi.sw.minidisc.exceptions;

public class ValueOutOfBoundsException extends Exception {

    public ValueOutOfBoundsException() {
        super("Given value was either too high or too low.");
    }
}
