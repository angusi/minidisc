/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package net.angusi.sw.minidisc.gui;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import net.angusi.sw.minidisc.MiniDisc;

public class ModePicker{

    private static ModePicker classHandle;
    private final Stage stage;


    private ModePicker() {
        System.out.println("Setting up Mode Picker stage");
        stage = new Stage();
        stage.setTitle(MiniDisc.getAppTitle()+" - Choose Mode");
        stage.setResizable(false);

        TilePane mainGroup = new TilePane();
        mainGroup.setPrefColumns(3);
        Scene scene = new Scene(mainGroup);
        stage.setScene(scene);

        Button cueWindowLaunchButton = new Button("Cue Player");
        cueWindowLaunchButton.setGraphic(new ImageView(new Image(ModePicker.class.getResourceAsStream("/net/angusi/sw/minidisc/res/icons/gostopfadeicon.png"))));
        cueWindowLaunchButton.setContentDisplay(ContentDisplay.TOP);
        cueWindowLaunchButton.setOnAction(event -> {
            CueWindow.getStage().show();
            stage.hide();
        });
        mainGroup.getChildren().add(cueWindowLaunchButton);

        Button cartWindowLaunchButton = new Button("Cart Player");
        cartWindowLaunchButton.setGraphic(new ImageView(new Image(ModePicker.class.getResourceAsStream("/net/angusi/sw/minidisc/res/icons/cartplayericon.png"))));
        cartWindowLaunchButton.setContentDisplay(ContentDisplay.TOP);
        cartWindowLaunchButton.setOnAction(event -> stage.hide());
        cartWindowLaunchButton.setDisable(true);
        mainGroup.getChildren().add(cartWindowLaunchButton);

        Button quitButton = new Button("Exit");
        quitButton.setGraphic(new ImageView(new Image(ModePicker.class.getResourceAsStream("/net/angusi/sw/minidisc/res/icons/quiticon.png"))));
        quitButton.setContentDisplay(ContentDisplay.TOP);
        quitButton.setOnAction(event -> MiniDisc.quitApplication());
        mainGroup.getChildren().add(quitButton);

        stage.sizeToScene();
        stage.centerOnScreen();
    }

    public static Stage getStage() {
        if(classHandle == null) {
            classHandle = new ModePicker();
        }
        return classHandle.stage;
    }
}
